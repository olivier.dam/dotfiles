#!/bin/bash

extensions=(
  akamud.vscode-theme-onedark
  wayou.vscode-todo-highlight
  vscode-icons-team.vscode-icons
  DavidAnson.vscode-markdownlint
  esbenp.prettier-vscode
  bierner.markdown-emoji
  waderyan.gitblame
  aaron-bond.better-comments
  timonwong.shellcheck
  HashiCorp.terraform
  gitlab.gitlab-workflow
  golang.go
  ms-kubernetes-tools.vscode-kubernetes-tools
  redhat.vscode-yaml
  mhutchie.git-graph
  eamodio.gitlens
)

if ! [ -x "$(command -v code)" ]; then
  echo 'Error: code is not installed.' >&2
else
  for ext in "${extensions[@]}"; do
    code --install-extension "$ext"
  done
fi
