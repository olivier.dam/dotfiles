#!/bin/bash

components=(
  kubectl
  gke-gcloud-auth-plugin
  beta
)

if ! [ -x "$(command -v gcloud)" ]; then
  echo 'Error: gcloud is not installed.' >&2
else
  gcloud components update --quiet
  for component in "${components[@]}"; do
    gcloud components install "$component"
  done
fi

if [ ! -f "/usr/local/bin/kubectl" ]; then
  echo 'Creating kubectl symlink'
  sudo ln -s /opt/homebrew/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/bin/kubectl /usr/local/bin
fi

if [ ! -f "/usr/local/bin/gke-gcloud-auth-plugin" ]; then
  echo 'Creating gke-gcloud-auth-plugin symlink'
  sudo ln -s /opt/homebrew/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/bin/gke-gcloud-auth-plugin /usr/local/bin
fi
